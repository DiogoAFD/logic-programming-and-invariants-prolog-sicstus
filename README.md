# **Health Institutions** #
## **Logic Programming and Invariants** ##

The project is about event registration in health institutions, using logic programming, specifically Prolog language, SICStus 4.3.0 version.

It was created a data structure, structural and referencial invariants that led to an evolution (or regression) of consistent knowledge, designed to overcome the limitations of predicates available in the libraries of language.